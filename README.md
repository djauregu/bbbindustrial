# BBBIndustrial

Getting started guide on the BeagleBoneBlack Industrial Rev B (Changed the processor to the AM3358BZCZ100.).

[Reference Manual.](https://github.com/beagleboard/beaglebone-black/wiki/System-Reference-Manual)

I will be following this [guide](https://beagleboard.org/getting-started#update) from my laptop with Archlinux as host.
My goal is get familiar with this popular board and used to test the performance of a SPI-CAN IC and also SPI communication over Linux.

## Board and pinout

![Board](Beaglebone-Black.jpg)

![Pinout](Beaglebone-Black-Pinout.png)

## uSD boot powered from the USB port in my laptop

* [Download image](http://beagleboard.org/latest-images/)

* Flash the image to the mini SD card. I used [Balena-ether](https://www.balena.io/etcher/) as they recommended, but installed from the [AUR](https://aur.archlinux.org/packages/balena-etcher/). For cli use: `xz -cd BBB-*.img.xz > /dev/mmcblk0`, check the name of the sd card with `lsblk`.

* Install this [udev rule](http://beagleboard.org/static/Drivers/Linux/FTDI/mkudevrule.sh) in the host.

* Connect the uSD card in the BBB, press the S2 (BOOT) botton an connect to the host throught a USB port. With `lsusb` and `ip a` the BBB interfaces can be seen. The ip address of the BBB should be `192.168.7.2` and my computer have been assigned with `192.168.7.1`. I still can access to the internet through the wifi connection of my laptop

* Open a terminal an `$ ssh 192.168.7.2 -l debian`, Also you have user `debian` and password `temppwd`. `sudo su` for sudo access (same password).

The results of my installation `# lsblk`
```
NAME         MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
mmcblk0      179:0    0 14.9G  0 disk 
└─mmcblk0p1  179:1    0  3.5G  0 part /
mmcblk1      179:8    0  3.6G  0 disk 
└─mmcblk1p1  179:9    0  3.6G  0 part 
mmcblk1boot0 179:16   0    4M  1 disk 
mmcblk1boot1 179:24   0    4M  1 disk 
```

It can be observer the 16GB uSD card (only with a 4GB partition) and the internal 4GB eMMC Flash memory.

To use the whole microSD card I followed these instructions:
```
# cd /opt/scripts/tools/
# git pull
# ./grow_partition.sh
# reboot
```
Check the result:
```
# lsblk
NAME         MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
mmcblk0      179:0    0 14.9G  0 disk 
└─mmcblk0p1  179:1    0 14.9G  0 part /
mmcblk1      179:8    0  3.6G  0 disk 
└─mmcblk1p1  179:9    0  3.6G  0 part 
mmcblk1boot0 179:16   0    4M  1 disk 
mmcblk1boot1 179:24   0    4M  1 disk 
```
### Timezone
```
# timedatectl set-timezone Europe/Stockholm
# date

Sun 07 Jun 2020 11:32:55 AM CEST
```

## Connect the BBB to the internet via USB connection

It is better to connect the 5V power supply at this stage.

## In the BBB

### One time

`$ ip a` display the network interfaces, `usb0` is the one we are using.

`# route add default gw 192.168.7.1` route the network traffic of the BBB to the host. Needs to be done everytime.

`# echo "nameserver 8.8.8.8" >> /etc/resolv.conf` The IP address 8.8.8.8 is the google.com main site and it is always online and it will return pings.

### Set for automatically every start up

`$ sudo nano /etc/network/interfaces`

```
iface usb0 inet static
    address 192.168.7.2
    netmask 255.255.255.252
    network 192.168.7.0
    gateway 192.168.7.1
    dns-nameservers 8.8.8.8 # not working
    post-up route add default gw 192.168.7.1
```

`$ sudo reboot`, connect again via ssh

`$ route` check if the gateway is set

`$ cat /etc/resolv.conf` check the `nameserver 8.8.8.8` is there. if not, `# echo "nameserver 8.8.8.8" >> /etc/resolv.conf`

These steps worked except the dns nameservers, for that I did:

`$ sudo apt-get install resolvconf`

`$ sudo nano /etc/resolvconf/resolv.conf.d/base` and add `nameserver 8.8.8.8`

`$ sudo resolvconf -u`

It should work the next reboot.


### In the Archlinux host (Share internet)

Reference in the [Archlinux wiki](https://wiki.archlinux.org/index.php/Internet_sharing)

`enp0s20f0u1u3` is the network interface of 192.168.7.1, and `wlp2s0` is the wifi interface of my laptop.

```
# ip link set dev enp0s20f0u1u3 up
# sysctl net.ipv4.ip_forward=1
# iptables --table nat --append POSTROUTING --out-interface wlp2s0 -j MASQUERADE
# iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
# iptables --append FORWARD --in-interface enp0s20f0u1u3 -j ACCEPT
```

Useful commands for testing:

`$ route`, `$ ping 8.8.8.8` and `$ ping www.google.com` in the BBB

`# tcpdump -ni wlp2s0 icmp`, `# tcpdump -ni enp0s20f0u1u3 icmp` in the host for checking the tcp traffic.

Need to be done on exact restart of the laptop.


## Install Debian in the eMMC Flash memory

* Flash the SD card with an [ready-for-flashing (prebuilt) BBB image](https://elinux.org/BeagleBoardDebian#eMMC:_All_BeagleBone_Variants_with_eMMC), or modifiy the `/boot/uEnv.txt` uncommenting the line `cmdline=init=/opt/scripts/tools/eMMC/init-eMMC-flasher-v3.sh` [Reference](https://elinux.org/Beagleboard:BeagleBoneBlack_Debian#Debian_Image_Testing_Snapshots).

Insert micro SD card into the (non-powered) BBB and then apply the power. Beaglebone four onboard LEDs flash back and forth in a “cylon” or “knight rider” pattern. During this time, the micro SD card program is flashing the onboard eMMC automatically for you. If you have an FTDI to USB adapter that fits onto J1, you can watch this process via the screen program. The automatic flashing to eMMC process should complete in about 10-20 minutes. When it’s done, for Debian Wheezy (7.x) the BBB shuts itself down (no LEDs on).  For Debian Jessie (8.x) the BBB 4 LEDs are all on constantly (instead of the cylon pattern). [Reference](https://www.scivision.dev/install-debian-7-to-emmc-internal-flash-drive-of-beaglebone-black/)

Once the BBB has shut down, REMOVE the micro SD card from the BBB. Reset the power or push the onboard POWER button next to the Ethernet jack.

After the install, the sd card could be used as additional mass storage. Upgrade the partition with `fdisk`. Format `$ sudo mkdosfs /dev/mmcblk0p1 -F32`, mount to /home/debian/sdcard `$ sudo mount /dev/mmcblk0p1 ~/sdcard`

Add: entry to the fstab file to automount 

### Serial 

Connect the board with the serial interface

| Board 	    |   My cable            | 
|---------------|-----------------------|
| Pin 1 (GND) 	|   Pin 5 (GND) - Black |
| Pin 4 (RX)    |   Pin 3 (TX)  - Green |
| Pin 5 (TX)    |   Pin 2 (RX)  - White |

`$ sudo screen /dev/ttyUSB0 115200` if using a USB-to-TTL 3.3V converter cable (not the same as USB-to-RS232, different voltage levels).

**For the CP210X chip based Serial Communication cable**

Cable are four wire - *red power, black ground*, *white RX into USB port*, and *green TX out of the USB port*. The power pin provides the 5V @ 500mA direct from the USB port and the RX/TX pins are 3.3V level for interfacing with the most common 3.3V logic level chipsets.

It should be able to see messages at the start up of the board.

**Reference**

https://elinux.org/Beagleboard:BeagleBone_Black_Serial

https://en.wikibooks.org/wiki/Serial_Programming/RS-232_Connections#Wiring_Pins_Explained

https://www.dummies.com/computers/beaglebone/how-to-connect-the-beaglebone-black-via-serial-over-usb/




### Setting up networking with the Ethernet port

https://www.ofitselfso.com/Beagle/NetworkingSetupConnectingTheBeagleboneBlack.php


### Install Archlinux into the BBB

https://archlinuxarm.org/platforms/armv7/ti/beaglebone-black


### Work with SPI

See the kernel version, `# uname -a`

`Linux beaglebone 4.19.94-ti-r42 #1buster SMP PREEMPT Tue Mar 31 19:38:29 UTC 2020 armv7l GNU/Linux`

https://e2e.ti.com/support/processors/f/791/p/714206/2647707#2647707?jktype=e2e

https://github.com/beagleboard/beaglebone-black/wiki/System-Reference-Manual


## Install Docker (amrhf)

https://nickjanetakis.com/blog/understanding-how-the-docker-daemon-and-docker-cli-work-together

https://docs.docker.com/engine/install/debian/

`$ sudo docker run hello-world`

`$ sudo usermod -aG docker your-user`

## Install Docker compose

No official Linux-amrv71, try this instead:

https://www.berthon.eu/2019/revisiting-getting-docker-compose-on-raspberry-pi-arm-the-easy-way/

## Setting docker to save images in sdcard

Research: Check how to mount the sdcard to save the docker image and also to log data

`$ mount`

To see the actual partitions in the system. The docker images could be seen there.

https://stackoverflow.com/questions/52488300/how-to-change-root-dir-of-docker-on-ubuntu-18-04-lts-docker-change-location-of

https://foxutech.com/how-to-change-docker-image-default-installation-directory/

https://www.freecodecamp.org/news/where-are-docker-images-stored-docker-container-paths-explained/



## Check speed of the eMMC and SD card
```
$ sudo apt install hdparm
$ sudo hdparm -tT /dev/mmcblk0 /dev/mmcblk1

/dev/mmcblk0:
 Timing cached reads:   488 MB in  2.00 seconds = 243.52 MB/sec
 HDIO_DRIVE_CMD(identify) failed: Invalid argument
 Timing buffered disk reads:  66 MB in  3.04 seconds =  21.68 MB/sec

/dev/mmcblk1:
 Timing cached reads:   504 MB in  2.00 seconds = 251.46 MB/sec
 HDIO_DRIVE_CMD(identify) failed: Invalid argument
 Timing buffered disk reads: 126 MB in  3.01 seconds =  41.80 MB/sec
```

Connect a USB Flash drive into the USB 2.0 port of the BBB
```
$ lsusb
Bus 001 Device 002: ID 0951:1666 Kingston Technology DataTraveler 100 G3/G4/SE9 G2
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub

$ sudo hdparm -tT /dev/sda1

/dev/sda1:
 Timing cached reads:   422 MB in  2.00 seconds = 210.54 MB/sec
SG_IO: bad/missing sense data, sb[]:  70 00 05 00 00 00 00 0a 00 00 00 00 20 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
 Timing buffered disk reads:  30 MB in  3.18 seconds =   9.43 MB/sec
```

Comparing with my laptop

```
$ sudo hdparm -tT /dev/nvme0n1

/dev/nvme0n1:
 Timing cached reads:   27344 MB in  1.99 seconds = 13761.84 MB/sec
 HDIO_DRIVE_CMD(identify) failed: Inappropriate ioctl for device
 Timing buffered disk reads: 3922 MB in  3.00 seconds = 1307.33 MB/sec
```

Same USB Flash drive into the USB 3.0 port in my laptop
```
$ sudo hdparm -tT /dev/sdb

/dev/sdb:
 Timing cached reads:   27402 MB in  1.99 seconds = 13789.97 MB/sec
SG_IO: bad/missing sense data, sb[]:  70 00 05 00 00 00 00 0a 00 00 00 00 20 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
 Timing buffered disk reads: 400 MB in  3.01 seconds = 132.85 MB/sec
```

This information is useful in you think to use the BBB board for intensive R/W applications like e.g. video related applications

Improve the performance in eMMC and SD card:

* Use RamFS (tmpfs) for the /tmp directory and even the /var/log files (but the log files will not be accesible after reboot-errors).



* Mount the SD card with "noatime" for avoiding reading while writing in this way the r/w cycles will be decresead. Add the SD card for execution of external applications like docker images and added to the `/etc/fstab` for automount.
```
...
tmpfs /tmp tmpfs size=100M 0 0
/dev/mmcblk0p1 /media/store ext4 noatime,defaults,nofail,user,auto 0 0
```
After any update in the `/etc/fstab`, do and `sudo mount -a` to check that works, if you get and error, corrrect it otherwise the BBB will not boot again after reboot.

Using the UUID could be better to customize the mounting point in case the several SD cards are used with the BBB. To obtain the UUID:
```
$ sudo blkid /dev/mmcblk0p1
/dev/mmcblk0p1: UUID="c9e09a37-3478-4309-8904-86f36f7ca69d"
TYPE="ext4" PARTUUID="0736b542-01"
```
And then
```
...
UUID=c9e09a37-3478-4309-8904-86f36f7ca69d /media/store ext4 defaults,nofail,user,auto 0 0
```

### Shutdown & Reboot

`$ sudo shutdown -h now`

`$ shutdown -h +5`

`$ sudo reboot`


# Accessing PRU

